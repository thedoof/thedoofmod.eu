---
audioDuration: “00:03:00”
audioFile: “straw.mp3”
title: "Credits"
---

Thanks to:
netlify for deploying
gohugo for static site generator
Zachary Betz for the PaperCSS theme
and to me for working hard on it!
![THANKS!](/thankyousomuch.jpeg)